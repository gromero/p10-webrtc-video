import argparse
import asyncio
import logging
import os
import json
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaPlayer


async def run(pc, player, role):
    if role == "offer":
        pc.addTrack(player.video)
        offer = await pc.createOffer()
        await pc.setLocalDescription(offer)

        # Imprimir el documento SDP del cliente para copiar y pegar
        print("SDP offer from client (copy and paste to the server):")
        print(json.dumps({"sdp": pc.localDescription.sdp, "type": pc.localDescription.type}))

        # Esperar a que el usuario pegue la respuesta SDP del servidor
        answer_json = input("Paste the SDP answer from the server (in JSON format): ")
        answer = json.loads(answer_json)
        await pc.setRemoteDescription(RTCSessionDescription(sdp=answer["sdp"], type=answer["type"]))

    while True:
        await asyncio.sleep(1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument("role", choices=["offer", "answer"])
    parser.add_argument("--play-from", help="Read the media from a file and sent it.")
    parser.add_argument("--verbose", "-v", action="count")
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    pc = RTCPeerConnection()

    # Verificar si el archivo de entrada existe
    if args.play_from and os.path.exists(args.play_from):
        player = MediaPlayer(args.play_from)
    else:
        print(f"Error: El archivo {args.play_from} no existe o no es accesible.")
        exit(1)

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(
            run(pc=pc, player=player, role=args.role)
        )
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())

#Comandos del cliente que pide un video en otro formato, lo manda en webm(offer)
# python3 client_video.py offer --play-from video.webm