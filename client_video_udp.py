import argparse
import asyncio
import json
import os
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaPlayer


class UDPSignaling(asyncio.DatagramProtocol):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.transport = None
        self.response_future = None

    async def connect(self):
        loop = asyncio.get_running_loop()
        self.transport, _ = await loop.create_datagram_endpoint(
            lambda: self,
            remote_addr=(self.host, self.port))
        await self.send("REGISTER CLIENT")

    async def send(self, message):
        if isinstance(message, dict):
            message = json.dumps(message)
        self.transport.sendto(message.encode())

    def datagram_received(self, data, addr):
        message = json.loads(data.decode())
        if self.response_future and not self.response_future.done():
            self.response_future.set_result(message)

    async def receive(self):
        self.response_future = asyncio.get_running_loop().create_future()
        return await self.response_future


async def run_offer(pc, player, signaling):
    await signaling.connect()
    pc.addTrack(player.video)

    offer = await pc.createOffer()
    await pc.setLocalDescription(offer)
    await signaling.send({"sdp": pc.localDescription.sdp, "type": pc.localDescription.type})

    while True:
        message = await signaling.receive()
        if "sdp" in message:
            desc = RTCSessionDescription(sdp=message["sdp"], type=message["type"])
            await pc.setRemoteDescription(desc)
        elif message == "BYE":
            break


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument("role", choices=["offer", "answer"])
    parser.add_argument("--play-from", help="Read the media from a file and send it.")
    parser.add_argument("--signalling-address", help="Signalling server address", default="127.0.0.1:9999")
    parser.add_argument("--verbose", "-v", action="count")
    args = parser.parse_args()

    if args.verbose:
        import logging

        logging.basicConfig(level=logging.DEBUG)

    signalling_host, signalling_port = args.signalling_address.split(":")
    signalling_address = (signalling_host, int(signalling_port))

    pc = RTCPeerConnection()

    if args.play_from and os.path.exists(args.play_from):
        player = MediaPlayer(args.play_from)
    else:
        print(f"Error: El archivo {args.play_from} no existe o no es accesible.")
        exit(1)

    signaling = UDPSignaling(signalling_host, int(signalling_port))

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run_offer(pc, player, signaling))
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        loop.run_until_complete(signaling.transport.close())
#Comandos del cliente que pide un video en otro formato,lo gestiona el signaling, manda webm(offer)
# python3 client_video_udp.py offer --play-from video.webm