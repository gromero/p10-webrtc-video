import argparse
import asyncio
import json
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaRecorder


class UDPSignaling(asyncio.DatagramProtocol):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.transport = None
        self.response_future = None

    async def connect(self):
        loop = asyncio.get_running_loop()
        self.transport, _ = await loop.create_datagram_endpoint(
            lambda: self,
            remote_addr=(self.host, self.port))
        await self.send("REGISTER SERVER")

    async def send(self, message):
        if isinstance(message, dict):
            message = json.dumps(message)
        self.transport.sendto(message.encode())

    def datagram_received(self, data, addr):
        message = json.loads(data.decode())
        if self.response_future and not self.response_future.done():
            self.response_future.set_result(message)

    async def receive(self):
        self.response_future = asyncio.get_running_loop().create_future()
        return await self.response_future


async def run_answer(pc, recorder, signaling):
    await signaling.connect()

    @pc.on("track")
    def on_track(track):
        recorder.addTrack(track)

    while True:
        message = await signaling.receive()
        if "sdp" in message:
            desc = RTCSessionDescription(sdp=message["sdp"], type=message["type"])
            await pc.setRemoteDescription(desc)
            if message["type"] == "offer":
                await pc.setLocalDescription(await pc.createAnswer())
                await signaling.send({"sdp": pc.localDescription.sdp, "type": pc.localDescription.type})
                await recorder.start()
        elif message == "BYE":
            break


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument("role", choices=["offer", "answer"])
    parser.add_argument("--record-to", help="Write received media to a file.")
    parser.add_argument("--signalling-address", help="Signalling server address", default="127.0.0.1:9999")
    parser.add_argument("--verbose", "-v", action="count")
    args = parser.parse_args()

    if args.verbose:
        import logging

        logging.basicConfig(level=logging.DEBUG)

    signalling_host, signalling_port = args.signalling_address.split(":")
    signalling_address = (signalling_host, int(signalling_port))

    pc = RTCPeerConnection()

    if args.record_to and not args.record_to.endswith('.mp4'):
        print("Error: El archivo de salida debe tener una extensión .mp4")
        exit(1)

    recorder = MediaRecorder(args.record_to)

    signaling = UDPSignaling(signalling_host, int(signalling_port))

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run_answer(pc, recorder, signaling))
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(recorder.stop())
        loop.run_until_complete(pc.close())
        loop.run_until_complete(signaling.transport.close())

# Comandos del servidor que respondecon  un video en el formato solicitado por el cliente, lo gestiona el signalling, .mp4(answer), es necesario "ctrl c", es decir terminar las conexiones para porder reproducir el video correctamente
# python3 server_video_udp.py answer --record-to video-out4.mp4