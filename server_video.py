import argparse
import asyncio
import logging
import os
import json
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaRecorder

async def run(pc, recorder, role):
    @pc.on("track")
    def on_track(track):
        print("Receiving %s" % track.kind)
        recorder.addTrack(track)

    if role == "answer":
        # Esperar a que el usuario pegue la oferta SDP del cliente
        offer_json = input("Paste the SDP offer from the client (in JSON format): ")
        offer = json.loads(offer_json)
        await pc.setRemoteDescription(RTCSessionDescription(sdp=offer["sdp"], type=offer["type"]))

        answer = await pc.createAnswer()
        await pc.setLocalDescription(answer)

        # Imprimir el documento SDP del servidor para copiar y pegar
        print("SDP answer from server (copy and paste to the client):")
        print(json.dumps({"sdp": pc.localDescription.sdp, "type": pc.localDescription.type}))

        await recorder.start()

    while True:
        await asyncio.sleep(1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument("role", choices=["offer", "answer"])
    parser.add_argument("--record-to", help="Write received media to a file.")
    parser.add_argument("--verbose", "-v", action="count")
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    pc = RTCPeerConnection()

    # Verificar si el archivo de salida tiene una extensión adecuada
    if args.record_to and not args.record_to.endswith('.mp4'):
        print("Error: El archivo de salida debe tener una extensión .mp4")
        exit(1)

    recorder = MediaRecorder(args.record_to)

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(
            run(pc=pc, recorder=recorder, role=args.role)
        )
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(recorder.stop())
        loop.run_until_complete(pc.close())
#Comandos del servidor que respondecon  un video en el formato solicitado por el cliente, .mp4(answer), es necesario "ctrl c", es decir terminar las conexiones para porder reproducir el video correctamente
# python3 server_video.py answer --record-to video-out3.mp4